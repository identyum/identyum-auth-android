![logo](https://docs.identyum.com/img/logo.png)


Identyum AppAuth for Android is a client SDK for communicating with
Identyum authentication service.

It strives to
directly map the requests and responses of those specifications, while following
the idiomatic style of the implementation language. In addition to mapping the
raw protocol flows, convenience methods are available to assist with common
tasks like performing an action with fresh tokens.

## Download

AppAuth for Android is available on [Maven](https://af.dev.identyum.com/artifactory/identyum-auth)

```groovy

repositories {
    maven { url "https://af.dev.identyum.com/artifactory/identyum-auth" }
}

implementation 'com.identyum:appauth:0.0.3'
```

## Requirements

Identyum AppAuth supports Android API 16 (Jellybean) and above. Browsers which provide a custom tabs
implementation are preferred by the library, but not required.
Both Custom URI Schemes (all supported versions of Android) and App Links (Android M / API 23+) can
be used with the library.

## Conceptual overview

Identyum AppAuth encapsulates the authorization state of the user in the
[com.identyum.appauth.AuthState](hhttps://bitbucket.org/identyum/identyum-auth-android//master/library/java/com/identyum/appauth/AuthState.java)
class, and communicates with an authorization server through the use of the
[com.identyum.appauth.AuthorizationService](hhttps://bitbucket.org/identyum/identyum-auth-android//master/library/java/com/identyum/appauth/AuthorizationService.java)
class.


Authorizing the user occurs via the user's web browser, and the request
is described using instances of
[AuthorizationRequest](hhttps://bitbucket.org/identyum/identyum-auth-android//master/library/java/com/identyum/appauth/AuthorizationRequest.java).
The request is dispatched using
[performAuthorizationRequest()](hhttps://bitbucket.org/identyum/identyum-auth-android//master/library/java/com/identyum/appauth/AuthorizationService.java) on an AuthorizationService instance, and the response (an
[AuthorizationResponse](hhttps://bitbucket.org/identyum/identyum-auth-android//master/library/java/com/identyum/appauth/AuthorizationResponse.java) instance) will be dispatched to the activity of your choice,
expressed via an Intent.

Token requests, such as obtaining a new access token using a refresh token,
follow a similar pattern:
[TokenRequest](hhttps://bitbucket.org/identyum/identyum-auth-android//master/library/java/com/identyum/appauth/TokenRequest.java)
[TokenResponse](hhttps://bitbucket.org/identyum/identyum-auth-android//master/library/java/com/identyum/appauth/TokenResponse.java)
instance is returned via a callback.

Responses can be provided to the
[update()](hhttps://bitbucket.org/identyum/identyum-auth-android//master/library/java/com/identyum/appauth/AuthState.java)
methods on AuthState in order to track and persist changes to the authorization
state. Once in an authorized state, the
[performActionWithFreshTokens()](hhttps://bitbucket.org/identyum/identyum-auth-android//master/library/java/com/identyum/appauth/AuthState.java)
method on AuthState can be used to automatically refresh access tokens
as necessary before performing actions that require valid tokens.

## Implementing the authorization code flow

It is recommended that native apps use the
[authorization code](https://tools.ietf.org/html/rfc6749#section-1.3.1)
flow with a public client to gain authorization to access user data. This has
the primary advantage for native clients that the authorization flow, which
must occur in a browser, only needs to be performed once.

This flow is effectively composed of four stages:

1. Discovering or specifying the endpoints to interact with the provider.
2. Authorizing the user, via a browser, in order to obtain an authorization
   code.
3. Exchanging the authorization code with the authorization server, to obtain
   a refresh token and/or ID token.
4. Using access tokens derived from the refresh token to interact with a
   resource server for further access to user data.

At each step of the process, an AuthState instance can (optionally) be updated
with the result to help with tracking the state of the flow.

### Authorization service configuration

First, AppAuth must be instructed how to interact with the authorization
service. This can be done  by directly creating an
[AuthorizationServiceConfiguration](hhttps://bitbucket.org/identyum/identyum-auth-android//master/library/java/com/identyum/appauth/AuthorizationServiceConfiguration.java)

Directly specifying an AuthorizationServiceConfiguration involves
invoking one of two possible static constructors

```java
AuthorizationServiceConfiguration serviceConfig =
    AuthorizationServiceConfiguration.sandbox(); // for sandbox environment

```

or

```java
AuthorizationServiceConfiguration serviceConfig =
    AuthorizationServiceConfiguration.prod(); // for production environment

```

If desired, this configuration can be used to seed an AuthState instance,
to persist the configuration easily:

```java
AuthState authState = new AuthState(serviceConfig);
```

### Obtaining an authorization code

An authorization code can now be acquired by constructing an
AuthorizationRequest, using its Builder. In AppAuth, the builders for each
data class accept the mandatory parameters via the builder constructor:

```java
AuthorizationRequest.Builder authRequestBuilder =
    new AuthorizationRequest.Builder(
        serviceConfig, // the authorization service configuration
        MY_CLIENT_ID, // the client ID, typically pre-registered and static
        ResponseTypeValues.CODE, // the response_type value: we want a code
        MY_REDIRECT_URI); // the redirect URI to which the auth response is sent
```


This request can then be dispatched using one of two approaches.

a `startActivityForResult` call using an Intent returned from the
`AuthorizationService`, or by calling `performAuthorizationRequest` and
providing pending intent for completion and cancelation handling activities.

The `startActivityForResult` approach is simpler to use but may require
more processing of the result:

```java
private void doAuthorization() {
  AuthorizationService authService = new AuthorizationService(this);
  Intent authIntent = authService.getAuthorizationRequestIntent(authRequest);
  startActivityForResult(authIntent, RC_AUTH);
}

@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data) {
  if (requestCode == RC_AUTH) {
    AuthorizationResponse resp = AuthorizationResponse.fromIntent(data);
    AuthorizationException ex = AuthorizationException.fromIntent(data);
    // ... process the response or exception ...
  } else {
    // ...
  }
}
```

If instead you wish to directly transition to another activity on completion
or cancelation, you can use `performAuthorizationRequest`:

```java
AuthorizationService authService = new AuthorizationService(this);

authService.performAuthorizationRequest(
    authRequest,
    PendingIntent.getActivity(this, 0, new Intent(this, MyAuthCompleteActivity.class), 0),
    PendingIntent.getActivity(this, 0, new Intent(this, MyAuthCanceledActivity.class), 0));
```

The intents may be customized to carry any additional data or flags required
for the correct handling of the authorization response.

#### Capturing the authorization redirect

Once the authorization flow is completed in the browser, the authorization
service will redirect to a URI specified as part of the authorization request,
providing the response via query parameters. In order for your app to
capture this response, it must register with the Android OS as a handler for
this redirect URI.

We recommend using a custom scheme based redirect URI (i.e. those of form
"my.scheme:/path"), as this is the most widely supported across all versions
of Android. It is strongly recommended to use "reverse domain name notation",
which is a naming convention based on the domain name system, but where the
domain components are reversed. For example, if the web domain for your service
is "service.example.com", then the reverse domain name form to use for a
custom scheme would be "com.example.service". This is also, typically, the
convention used for the package name of your app, e.g. "com.example.app". As
such, the package name for your app can often be used as a custom scheme -
there are some exceptions, such as when the package name contains underscores,
as these are not legal characters for URI schemes.

When a custom scheme is used, AppAuth can be easily configured to capture
all redirects using this custom scheme through a manifest placeholder:

```groovy
android.defaultConfig.manifestPlaceholders = [
  'appAuthRedirectScheme': 'com.example.app'
]
```

Alternatively, the redirect URI can be directly configured by adding an
intent-filter for AppAuth's RedirectUriReceiverActivity to your
AndroidManifest.xml:

```xml
<activity
        android:name="com.identyum.appauth.RedirectUriReceiverActivity"
        tools:node="replace">
    <intent-filter>
        <action android:name="android.intent.action.VIEW"/>
        <category android:name="android.intent.category.DEFAULT"/>
        <category android:name="android.intent.category.BROWSABLE"/>
        <data android:scheme="com.example.app"/>
    </intent-filter>
</activity>
```

If an HTTPS redirect URI is required instead of a custom scheme, the same
approach (modifying your AndroidManifest.xml) is used:

```xml
<activity
        android:name="com.identyum.appauth.RedirectUriReceiverActivity"
        tools:node="replace">
    <intent-filter>
        <action android:name="android.intent.action.VIEW"/>
        <category android:name="android.intent.category.DEFAULT"/>
        <category android:name="android.intent.category.BROWSABLE"/>
        <data android:scheme="https"
              android:host="app.example.com"
              android:path="/oauth2redirect"/>
    </intent-filter>
</activity>
```

HTTPS redirects can be secured by configuring the redirect URI as an
[app link](https://developer.android.com/training/app-links/index.html) in
Android M and above. We recommend that a fallback page be configured at
the same address to forward authorization responses to your app via a custom
scheme, for older Android devices.

#### Handling the authorization response

Upon completion of the authorization flow, the completion Intent provided to
performAuthorizationRequest will be triggered. The authorization response
is provided to this activity via Intent extra data, which can be extracted
using the `fromIntent()` methods on AuthorizationResponse and
AuthorizationException respectively:

```java
public void onCreate(Bundle b) {
  AuthorizationResponse resp = AuthorizationResponse.fromIntent(getIntent());
  AuthorizationException ex = AuthorizationException.fromIntent(getIntent());
  if (resp != null) {
    // authorization completed
  } else {
    // authorization failed, check ex for more details
  }
  // ...
}
```

The response can be provided to the AuthState instance for easy persistence
and further processing:

```
authState.update(resp, ex);
```

If the full redirect URI is required in order to extract additional information
that AppAuth does not provide, this is also provided to your activity:

```java
public void onCreate(Bundle b) {
  // ...
  Uri redirectUri = getIntent().getData();
  // ...
}
```

### Exchanging the authorization code

Given a successful authorization response carrying an authorization code,
a token request can be made to exchange the code for a refresh token:

```java
authService.performTokenRequest(
    resp.createTokenExchangeRequest(),
    new AuthorizationService.TokenResponseCallback() {
      @Override public void onTokenRequestCompleted(
            TokenResponse resp, AuthorizationException ex) {
          if (resp != null) {
            // exchange succeeded
          } else {
            // authorization failed, check ex for more details
          }
        }
    });
```

The token response can also be used to update an AuthState instance:

```java
authState.update(resp, ex);
```

### Using access tokens

Finally, the retrieved access token can be used to interact with a resource
server. This can be done directly, by extracting the access token from a
token response. However, in most cases, it is simpler to use the
`performActionWithFreshTokens` utility method provided by AuthState:

```java
authState.performActionWithFreshTokens(service, new AuthStateAction() {
  @Override public void execute(
      String accessToken,
      AuthorizationException ex) {
    if (ex != null) {
      // negotiation for fresh tokens failed, check ex for more details
      return;
    }

    // use the access token to do something ...
  }
});
```



```java
AuthorizationService authService = new AuthorizationService(this);

authService.performEndSessionRequest(
    endSessionRequest,
    PendingIntent.getActivity(this, 0, new Intent(this, MyAuthCompleteActivity.class), 0),
    PendingIntent.getActivity(this, 0, new Intent(this, MyAuthCanceledActivity.class), 0));
```

End session flow will also work involving browser mechanism that is described in authorization
mechanism session.
Handling response mechanism with transition to another activity should be as follows:

 ```java
public void onCreate(Bundle b) {
  EndSessionResponse resp = EndSessionResponse.fromIntent(getIntent());
  AuthorizationException ex = AuthorizationException.fromIntent(getIntent());
  if (resp != null) {
    // authorization completed
  } else {
    // authorization failed, check ex for more details
  }
  // ...
}
```

### AuthState persistence

Instances of `AuthState` keep track of the authorization and token
requests and responses. This is the only object that you need to persist to
retain the authorization state of the session. Typically, one would do this by
storing the authorization state in SharedPreferences or some other persistent
store private to the app:

```java
@NonNull public AuthState readAuthState() {
  SharedPreferences authPrefs = getSharedPreferences("auth", MODE_PRIVATE);
  String stateJson = authPrefs.getString("stateJson", null);
  if (stateJson != null) {
    return AuthState.jsonDeserialize(stateJson);
  } else {
    return new AuthState();
  }
}

public void writeAuthState(@NonNull AuthState state) {
  SharedPreferences authPrefs = getSharedPreferences("auth", MODE_PRIVATE);
  authPrefs.edit()
      .putString("stateJson", state.jsonSerializeString())
      .apply();
}
```



### Controlling which browser is used for authorization

Some applications require explicit control over which browsers can be used
for authorization - for example, to require that Chrome be used for
second factor authentication to work, or require that some custom browser
is used for authentication in an enterprise environment.

Control over which browsers can be used can be achieved by defining a
[BrowserMatcher](hhttps://bitbucket.org/identyum/identyum-auth-android//master/library/java/com/identyum/appauth/browser/BrowserMatcher.java), and supplying this to the builder of AppAuthConfiguration.
A BrowserMatcher is suppled with a
[BrowserDescriptor](hhttps://bitbucket.org/identyum/identyum-auth-android//master/library/com/identyum/appauth/browser/BrowserDescriptor.java)
instance, and must decide whether this browser is permitted for the
authorization flow.

By default, [AnyBrowserMatcher](hhttps://bitbucket.org/identyum/identyum-auth-android//master/library/java/com/identyum/appauth/browser/AnyBrowserMatcher.java)
is used.

For your convenience, utility classes to help define a browser matcher are
provided, such as:

- [Browsers](hhttps://bitbucket.org/identyum/identyum-auth-android//master/library/java/com/identyum/appauth/browser/Browsers.java):
  contains a set of constants for the official package names and signatures
  of Chrome, Firefox and Samsung SBrowser.
- [VersionedBrowserMatcher](hhttps://bitbucket.org/identyum/identyum-auth-android//master/library/java/com/identyum/appauth/browser/VersionedBrowserMatcher.java):
  will match a browser if it has a matching package name and signature, and
  a version number within a defined
  [VersionRange](hhttps://bitbucket.org/identyum/identyum-auth-android//master/library/java/com/identyum/appauth/browser/VersionRange.java). This class also provides some static instances for matching
  Chrome, Firefox and Samsung SBrowser.
- [BrowserAllowList](hhttps://bitbucket.org/identyum/identyum-auth-android//master/library/java/com/identyum/appauth/browser/BrowserAllowList.java):
  takes a list of BrowserMatcher instances, and will match a browser if any
  of these child BrowserMatcher instances signals a match.
- [BrowserDenyList](hhttps://bitbucket.org/identyum/identyum-auth-android//master/library/java/com/identyum/appauth/browser/BrowserDenyList.java):
  the inverse of BrowserAllowList - takes a list of browser matcher instances,
  and will match a browser if it _does not_ match any of these child
  BrowserMatcher instances.

For instance, in order to restrict the authorization flow to using Chrome
or SBrowser as a custom tab:

```java
AppAuthConfiguration appAuthConfig = new AppAuthConfiguration.Builder()
    .setBrowserMatcher(new BrowserAllowList(
        VersionedBrowserMatcher.CHROME_CUSTOM_TAB,
        VersionedBrowserMatcher.SAMSUNG_CUSTOM_TAB))
    .build();
AuthorizationService authService =
        new AuthorizationService(context, appAuthConfig);
```

Or, to prevent the use of a buggy version of the custom tabs in
Samsung SBrowser:

```java
AppAuthConfiguration appAuthConfig = new AppAuthConfiguration.Builder()
    .setBrowserMatcher(new BrowserDenyList(
        new VersionedBrowserMatcher(
            Browsers.SBrowser.PACKAGE_NAME,
            Browsers.SBrowser.SIGNATURE_SET,
            true, // when this browser is used via a custom tab
            VersionRange.atMost("5.3")
        )))
    .build();
AuthorizationService authService =
        new AuthorizationService(context, appAuthConfig);
```

### Building from Android Studio

In AndroidStudio, File -> New -> Import project. Select the root folder
(the one with the `build.gradle` file).
