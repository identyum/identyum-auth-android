/*
 * Copyright 2015 The AppAuth for Android Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.identyum.appauth;

import android.net.Uri;

import androidx.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import static com.identyum.appauth.Preconditions.checkArgument;
import static com.identyum.appauth.Preconditions.checkNotNull;

/**
 * Configuration details required to interact with an authorization service.
 */
public class AuthorizationServiceConfiguration {

    /**
     * The standard base path for well-known resources on domains.
     *
     * @see "Defining Well-Known Uniform Resource Identifiers (RFC 5785)
     * <https://tools.ietf.org/html/rfc5785>"
     */
    public static final String WELL_KNOWN_PATH =
        ".well-known";

    /**
     * The standard resource under {@link #WELL_KNOWN_PATH .well-known} at which an OpenID Connect
     * discovery document can be found under an issuer's base URI.
     *
     * @see "OpenID Connect discovery 1.0
     * <https://openid.net/specs/openid-connect-discovery-1_0.html>"
     */
    public static final String OPENID_CONFIGURATION_RESOURCE =
        "openid-configuration";

    private static final String KEY_AUTHORIZATION_ENDPOINT = "authorizationEndpoint";
    private static final String KEY_TOKEN_ENDPOINT = "tokenEndpoint";

    private static final String AUTHORIZATION_ENDPOINT_DEV = "https://web-components.dev.identyum.com/authorize";
    private static final String TOKEN_ENDPOINT_DEV = "https://identifier.dev.identyum.com/api/v1/auth/token";

    private static final String AUTHORIZATION_ENDPOINT_SAND = "https://web-components.stage.identyum.com/authorize";
    private static final String TOKEN_ENDPOINT_SAND = "https://identifier.stage.identyum.com/api/v1/auth/token";

    private static final String AUTHORIZATION_ENDPOINT_PROD = "https://web-components.prod.identyum.com/authorize";
    private static final String TOKEN_ENDPOINT_PROD = "https://identifier.prod.identyum.com/api/v1/auth/token";

    /**
     * The authorization service's endpoint.
     */
    public final Uri authorizationEndpoint;

    /**
     * The authorization service's token exchange and refresh endpoint.
     */
    public final Uri tokenEndpoint;


    /**
     * Creates a service configuration for development environment.
     */
    public static AuthorizationServiceConfiguration dev() {
        return new AuthorizationServiceConfiguration(
            Uri.parse(AUTHORIZATION_ENDPOINT_DEV),
            Uri.parse(TOKEN_ENDPOINT_DEV)
        );
    }

    /**
     * Creates a service configuration for sandbox environment.
     */
    public static AuthorizationServiceConfiguration sandbox() {
        return new AuthorizationServiceConfiguration(
            Uri.parse(AUTHORIZATION_ENDPOINT_SAND),
            Uri.parse(TOKEN_ENDPOINT_SAND)

        );
    }

    /**
     * Creates a service configuration for production environment.
     */
    public static AuthorizationServiceConfiguration prod() {
        return new AuthorizationServiceConfiguration(
            Uri.parse(AUTHORIZATION_ENDPOINT_PROD),
            Uri.parse(TOKEN_ENDPOINT_PROD)
        );
    }

    private AuthorizationServiceConfiguration(
        @NonNull Uri authorizationEndpoint,
        @NonNull Uri tokenEndpoint) {
        this.authorizationEndpoint = checkNotNull(authorizationEndpoint);
        this.tokenEndpoint = checkNotNull(tokenEndpoint);
    }

    /**
     * Converts the authorization service configuration to JSON for storage or transmission.
     */
    @NonNull
    public JSONObject toJson() {
        JSONObject json = new JSONObject();
        JsonUtil.put(json, KEY_AUTHORIZATION_ENDPOINT, authorizationEndpoint.toString());
        JsonUtil.put(json, KEY_TOKEN_ENDPOINT, tokenEndpoint.toString());
        return json;
    }

    /**
     * Converts the authorization service configuration to a JSON string for storage or
     * transmission.
     */
    public String toJsonString() {
        return toJson().toString();
    }

    /**
     * Reads an Authorization service configuration from a JSON representation produced by the
     * {@link #toJson()} method or some other equivalent producer.
     *
     * @throws JSONException if the provided JSON does not match the expected structure.
     */
    @NonNull
    public static AuthorizationServiceConfiguration fromJson(@NonNull JSONObject json)
        throws JSONException {
        checkNotNull(json, "json object cannot be null");

            checkArgument(json.has(KEY_AUTHORIZATION_ENDPOINT), "missing authorizationEndpoint");
            checkArgument(json.has(KEY_TOKEN_ENDPOINT), "missing tokenEndpoint");
            return new AuthorizationServiceConfiguration(
                JsonUtil.getUri(json, KEY_AUTHORIZATION_ENDPOINT),
                JsonUtil.getUri(json, KEY_TOKEN_ENDPOINT)
            );
    }

    /**
     * Reads an Authorization service configuration from a JSON representation produced by the
     * {@link #toJson()} method or some other equivalent producer.
     *
     * @throws JSONException if the provided JSON does not match the expected structure.
     */
    public static AuthorizationServiceConfiguration fromJson(@NonNull String jsonStr)
        throws JSONException {
        checkNotNull(jsonStr, "json cannot be null");
        return AuthorizationServiceConfiguration.fromJson(new JSONObject(jsonStr));
    }


    static Uri buildConfigurationUriFromIssuer(Uri openIdConnectIssuerUri) {
        return openIdConnectIssuerUri.buildUpon()
            .appendPath(WELL_KNOWN_PATH)
            .appendPath(OPENID_CONFIGURATION_RESOURCE)
            .build();
    }

}
